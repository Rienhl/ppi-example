﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MovementType { Simple, Lerp, SmoothDamp }

public class BasicMovement : MonoBehaviour
{
	public MovementType movType;

	public float speed = 1f;
	public float smoothTime = 1;

	[SerializeField]
	private float xInput = 0;
	[SerializeField]
	private float currentSpeed = 0;

	private Vector3 smoothedVelocityRef;

	private void Update()
	{
		switch (movType)
		{
			case MovementType.Simple:
				SimpleMovement();
				break;
			case MovementType.Lerp:
				LerpedMovement();
				break;
			case MovementType.SmoothDamp:
				SmoothedMovement();
				break;
			default:
				break;
		}
	}

	private void SimpleMovement()
	{
		xInput = Input.GetAxisRaw("Horizontal");
		currentSpeed = xInput * speed;
		transform.position += transform.right * currentSpeed; //transform.right = (1, 0, 0)
	}

	private void LerpedMovement()
	{
		xInput = Input.GetAxisRaw("Horizontal");
		currentSpeed = xInput * speed;
		transform.position = Vector3.Lerp(transform.position, transform.position + transform.right * currentSpeed, Time.deltaTime);
	}

	private void SmoothedMovement()
	{
		xInput = Input.GetAxisRaw("Horizontal");
		currentSpeed = xInput * speed;
		Vector3 targetPos = transform.position + transform.right * currentSpeed;
		transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref smoothedVelocityRef, smoothTime);
	}
}

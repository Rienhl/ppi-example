﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BasicMovementUI : MonoBehaviour
{
	public Text xInputValue;
	public Text currentSpeedValue;
	public Text positionValue;

	private BasicMovement bm;

	private void Awake()
	{
		bm = GameObject.FindGameObjectWithTag("Tank").GetComponent<BasicMovement>();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelController : MonoBehaviour
{
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			Debug.Log("Barrel Pickup");
			MyGameManager.instance.AddScore(100);
			Destroy(gameObject);
		}
	}
}
